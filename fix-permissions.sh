#!/bin/bash

source .env

chown -R www-data:nc ${DATAROOT}
chmod -R u=rwX ${DATAROOT}
chmod -R g=rX ${DATAROOT}
chmod -R o= ${DATAROOT}
