# Nextcloud with SSL

## Data root

All nextcloud related non-static files reside under `${DATAROOT}` which is defind in the `env` file.

The hostname and domain	to be used are also defined in the `env` file.

Example	`env` file:

```
DATAROOT=/data/nextcloud/
mydomain=grahsl.net
myhostname=r19
```

*Caution:* When migrating from one server to another, the `trusted_domains` variable in `${DATAROOT}/nc/nextcloud/config/config.php` needs to be adjusted.

## Credentials

Credentials are configured through following files

- `godaddy.credentials`
- `db.credentials`

### Gandi

TBD

### Internal Postgres DB

Populate the 'rdb.credentials' file as follows

```
POSTGRES_PASSWORD=password
POSTGRES_USER=username
```

## Certificate handling

Letsencrypt is used to automatically create certificates

### Fix pending file locks

```
docker-compose exec --user root ncdb bash
psql -U postgres
/c nextcloud
DELETE FROM file_locks WHERE true;
```

### To rescan all files and index use:

```
docker-compose exec --user www-data nextcloud php occ files:scan --all
```

### To check cert status 

```
docker exec grahsl-letsencrypt-companion-1 /app/cert_status
```
/app/force_renew

# Simple docker registry

## TLS

The registry itself is running as ordinary http service. This repo relies on a running jrcs/letsencrypt-nginx-proxy-companion together with a nginx reverse proxy.

## Credentials

Create credentials file `auth/htpasswd`

`docker run --entrypoint htpasswd  registry:2.6.2 -Bbn <username> <password> > auth/htpasswd`

Note that this works only with version `2.6.2` as htpasswd was removed from the runtime container in higher versions

## Login

To use the registry you need to:

`docker login <hostname>`

## Push/Pull

`docker pull <hostname>/<repo>`

`docker push <hostname>/<repo>`

## Convert oggs to mp3

for i in *.ogg; do ffmpeg -i "$i" -acodec libmp3lame "mp3/${i%.*}.mp3"; done


## Backup to local disk

rsync -avrzu --delete /data/ /backup/data --exclude=work --progress
rsync -avrzu --delete /data/ root@bf:/backup/data --exclude=work --progress


docker-compose exec --user www-data nextcloud /bin/bash
docker-compose exec --user www-data nextcloud php occ maintenance:mode --off
