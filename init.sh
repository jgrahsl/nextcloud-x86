#!/bin/bash

set -x

source env

echo "Sure to erase and reinitialize this folder: ${DATAROOT:?No Dataroot given}"
read answer
if [ "$answer" != "${answer#[Yy]}" ] ;then
    echo "Proceeding ... "
else
    exit 0
fi

rm -fR ${DATAROOT}
mkdir -p ${DATAROOT}

mkdir ${DATAROOT}/db
mkdir ${DATAROOT}/nextcloud
mkdir ${DATAROOT}/certs
mkdir ${DATAROOT}/html
mkdir ${DATAROOT}/vhost.d

docker-compose rm
docker-compose build --pull
docker-compose up
